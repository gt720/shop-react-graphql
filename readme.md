```bash
prisma console
prisma deploy --env-file variables.env

heroku apps:create next-shop-yoga-prod
git subtree push --prefix backend heroku master
git remote add heroku-frontend https://git.heroku.com/next-shop.git
```
