const cookieParser = require("cookie-parser");
const jwt = require("jsonwebtoken");

require("dotenv").config({ path: "variables.env" });
const createServer = require("./createServer");
const db = require("./db");

const server = createServer();

//#region Middleware
server.express.use(cookieParser());
// decode the JWT
server.express.use((req, res, next) => {
  const { token } = req.cookies;
  if (token) {
    try {
      const { userId } = jwt.verify(token, process.env.APP_SECRET);
      // put the userId onto the req
      req.userId = userId;
    } catch (e) {}
  }
  next();
});
// get user
server.express.use(async (req, res, next) => {
  if (req.userId) {
    req.user = await db.query.user(
      { where: { id: req.userId } },
      "{ id, name, email, permissions }"
    );
  }
  next();
});

//#endregion

server.start(
  {
    cors: {
      credentials: true,
      origin: process.env.FRONTEND_URL
    }
  },
  deets => {
    console.log(`Server running on http://localhost:${deets.port}`);
  }
);
