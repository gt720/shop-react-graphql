const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const { randomBytes } = require("crypto");
const { promisify } = require("util");
const { transport, makeANiceEmail } = require("../mail");
const { hasPermission } = require("../utils");

const jwtGenerate = (context, userId) => {
  const token = jwt.sign({ userId }, process.env.APP_SECRET);
  context.response.cookie("token", token, {
    httpOnly: true,
    maxAge: 1000 * 60 * 60 * 24 * 365 // 1 year cookie
  });
};

const getUserOrFail = async (db, whereObj) => {
  const user = await db.query.user({ where: whereObj });
  if (!user) {
    throw new Error(`User with email: ${email} not found`);
  }

  return user;
};

// info return type
const mutations = {
  //#region ITEMS
  createItem(schema, arguments, context, info) {
    if (!context.request.userId) throw new Error("Вам необходимо войти");

    const item = context.db.mutation.createItem(
      {
        data: {
          user: {
            connect: {
              id: context.request.userId
            }
          },
          ...arguments
        }
      },
      info
    );

    return item;
  },
  updateItem(parent, args, context, info) {
    // first take a copy of the updates
    const updates = { ...args };
    // remove the ID from the updates
    delete updates.id;
    // run the update method
    return context.db.mutation.updateItem(
      { data: updates, where: { id: args.id } },
      info
    );
  },
  async deleteItem(parent, args, context, info) {
    const where = { id: args.id };
    const item = await context.db.query.item(
      { where },
      `{id, title, user{id, name, email}}`
    );
    // TODO check permission
    let userIsOwner = item.user.id === context.request.userId;
    if (!userIsOwner) throw new Error("You are not owner");
    hasPermission(context.request.user, ["ADMIN", "ITEMDELETE"]);

    return context.db.mutation.deleteItem({ where }, info);
  },
  //#endregion

  //#region AUTH
  async signup(parent, args, context, info) {
    args.email = args.email.toLowerCase();
    const password = await bcrypt.hash(args.password, 10);

    const user = await context.db.mutation.createUser(
      {
        data: {
          ...args,
          password,
          permissions: { set: ["USER"] }
        }
      },
      info
    );

    jwtGenerate(context, user.id);

    return user;
  },
  async signin(parent, args, context, info) {
    const email = args.email.toLowerCase();
    const user = await getUserOrFail(context.db, { email });
    const valid = await bcrypt.compare(args.password, user.password);
    if (!valid) {
      throw new Error("Invalid Password!");
    }

    jwtGenerate(context, user.id);

    return user;
  },
  signout(parent, args, ctx, info) {
    ctx.response.clearCookie("token");
    return { message: "Goodbye!" };
  },
  async updatePermissions(parent, args, context, info) {
    if (!context.request.userId) {
      throw new Error("You must be logged in!");
    }

    hasPermission(context.request.user, ["ADMIN", "PERMISSIONUPDATE"]);

    const [user] = await context.db.query.users(
      { where: { id: args.id } },
      info
    );
    console.log(user);
    if (!user) {
      throw new Error("User not found");
    }

    await context.db.mutation.updateUser({
      where: { id: user.id },
      data: {
        permissions: { set: args.permissions }
      }
    });

    return user;
  },
  //#endregion

  //#region Reset Password
  async resetRequest(parent, args, ctx, info) {
    const user = await getUserOrFail(ctx.db, { email: args.email });

    const randomBytesPromiseified = promisify(randomBytes);
    const resetToken = (await randomBytesPromiseified(20)).toString("hex");
    const resetTokenExpiry = Date.now() + 3600000; // 1 hour from now
    const res = await ctx.db.mutation.updateUser({
      where: { email: args.email },
      data: { resetToken, resetTokenExpiry }
    });

    // const mailRes = await transport.sendMail({
    //   from: process.env.MAIL_ADMIN,
    //   to: user.email,
    //   subject: "Your Password Reset Token",
    //   html: makeANiceEmail(`Your Password Reset Token is here!
    //   \n\n
    //   <a href="${
    //     process.env.FRONTEND_URL
    //   }/reset-password?resetToken=${resetToken}">Click Here to Reset</a>`)
    // });

    return { message: "Thanks!" };
  },

  async resetPassword(parent, args, ctx, info) {
    if (args.password !== args.confirmPassword) {
      throw new Error("Passwords don't match!");
    }

    const [user] = await ctx.db.query.users({
      where: {
        resetToken: args.resetToken,
        resetTokenExpiry_gte: Date.now() - 3600000
      }
    });

    if (!user) {
      throw new Error("This token is either invalid or expired!");
    }

    const password = await bcrypt.hash(args.password, 10);

    const updatedUser = await ctx.db.mutation.updateUser(
      {
        where: { email: user.email },
        data: {
          password,
          resetToken: null,
          resetTokenExpiry: null
        }
      },
      info
    );

    jwtGenerate(ctx, updatedUser.id);

    return updatedUser;
  },
  //#endregion

  //#region cart
  async addToCart(parent, args, ctx, info) {
    const { userId } = ctx.request;
    if (!userId) {
      throw new Error("Вам необходимо войти");
    }

    const [existingCartItem] = await ctx.db.query.cartItems({
      where: {
        user: { id: userId },
        item: { id: args.id }
      }
    });

    if (existingCartItem) {
      return ctx.db.mutation.updateCartItem(
        {
          where: { id: existingCartItem.id },
          data: { quantity: existingCartItem.quantity + 1 }
        },
        info
      );
    }

    return ctx.db.mutation.createCartItem(
      {
        data: {
          user: {
            connect: { id: userId }
          },
          item: {
            connect: { id: args.id }
          }
        }
      },
      info
    );
  },
  async removeFromCart(parent, args, ctx, info) {
    const cartItem = await ctx.db.query.cartItem(
      {
        where: {
          id: args.id
        }
      },
      `{ id, user { id }}`
    );

    if (!cartItem) throw new Error("На найдена позиция в корзине");

    if (cartItem.user.id !== ctx.request.userId) {
      throw new Error("Это не ваш товар");
    }

    return ctx.db.mutation.deleteCartItem(
      {
        where: { id: args.id }
      },
      info
    );
  },
  //#endregion

  async createOrder(parent, args, ctx, info) {
    const { userId } = ctx.request;
    if (!userId) {
      throw new Error("Вам необходимо войти");
    }

    const user = await ctx.db.query.user(
      { where: { id: userId } },
      `{
      id
      name
      email
      cart {
        id
        quantity
        item { title price id description image largeImage }
      }}`
    );

    const amount = user.cart.reduce(
      (tally, cartItem) => tally + cartItem.item.price * cartItem.quantity,
      0
    );

    const orderItems = user.cart.map(cartItem => {
      const orderItem = {
        ...cartItem.item,
        quantity: cartItem.quantity,
        user: { connect: { id: userId } }
      };
      delete orderItem.id;
      return orderItem;
    });

    const order = await ctx.db.mutation.createOrder({
      data: {
        total: amount,
        items: { create: orderItems },
        user: { connect: { id: userId } }
      }
    });

    const cartItemIds = user.cart.map(cartItem => cartItem.id);
    await ctx.db.mutation.deleteManyCartItems({
      where: {
        id_in: cartItemIds
      }
    });

    return order;
  }
};

module.exports = mutations;
