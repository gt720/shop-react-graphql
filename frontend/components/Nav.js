import Link from "next/link";
import User from "./User/User";
import { Mutation } from "react-apollo";
import {
  USER_CURRENT_QUERY,
  USER_SIGNOUT_MUTATION,
  LOCAL_CART_TOGGLE_MUTATION
} from "../lib/queries";
const Nav = () => (
  <User>
    {({ data }) => {
      return (
        <ul className="nav justify-content-center">
          <li className="nav-item">
            <Link href="/items">
              <a className="nav-link">Магазин</a>
            </Link>
          </li>

          {data.me && (
            <React.Fragment>
              <li className="nav-item">
                <a className="nav-link">
                  {data.me.name} {data.me.email}
                </a>
              </li>
              <li className="nav-item">
                <Link href="/sell">
                  <a className="nav-link">Продать</a>
                </Link>
              </li>

              <li className="nav-item">
                <Link href="/orders">
                  <a className="nav-link">Заказы</a>
                </Link>
              </li>

              <Mutation
                mutation={USER_SIGNOUT_MUTATION}
                refetchQueries={[{ query: USER_CURRENT_QUERY }]}
              >
                {signout => (
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link"
                      onClick={e => {
                        e.preventDefault();
                        signout();
                      }}
                    >
                      Выход
                    </a>
                  </li>
                )}
              </Mutation>

              <Mutation mutation={LOCAL_CART_TOGGLE_MUTATION}>
                {toggleCart => (
                  <li className="nav-item">
                    <a
                      href="#"
                      className="nav-link"
                      onClick={e => {
                        e.preventDefault();
                        toggleCart();
                      }}
                    >
                      Корзина{" "}
                      {data.me.cart.length > 0 && (
                        <span className="badge  badge-pill badge-danger">
                          {data.me.cart.reduce((count, cartItem) => {
                            return count + cartItem.quantity;
                          }, 0)}
                        </span>
                      )}
                    </a>
                  </li>
                )}
              </Mutation>
            </React.Fragment>
          )}
          {!data.me && (
            <React.Fragment>
              <li className="nav-item">
                <Link href="/signup">
                  <a className="nav-link">Регистрация</a>
                </Link>
              </li>
              <li className="nav-item">
                <Link href="/signin">
                  <a className="nav-link">Вход</a>
                </Link>
              </li>
            </React.Fragment>
          )}
        </ul>
      );
    }}
  </User>
);

export default Nav;
