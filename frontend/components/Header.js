import Nav from "./Nav";
import Link from "next/link";
import Router from "next/router";
import NProgress from "nprogress";
import Cart from "./Cart/Cart";
import { ToastContainer } from "react-toastify";
import Search from "./Items/Search";

Router.onRouteChangeStart = () => {
  NProgress.start();
};
Router.onRouteChangeComplete = () => {
  NProgress.done();
};
Router.onRouteChangeError = () => {
  NProgress.done();
};

const Header = () => (
  <header>
    <div className="container-fluid">
      <div className="row">
        <h1 className="logo">
          <Link href="/">
            <a>next shop</a>
          </Link>
        </h1>
      </div>
      <Nav />
      <Search />
    </div>
    <Cart />
    <ToastContainer />
  </header>
);

export default Header;
