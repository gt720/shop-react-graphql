import React, { Component } from "react";
import { Mutation } from "react-apollo";
import DisplayError from "../ErrorMessage";
import Spiner from "../Spiner";
import { USER_SIGNUP_MUTATION, USER_CURRENT_QUERY } from "../../lib/queries";

export default class SignupForm extends Component {
  state = {
    name: "",
    email: "",
    password: ""
  };
  saveToState = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="row mt-4 flex justify-content-center">
        <Mutation
          mutation={USER_SIGNUP_MUTATION}
          variables={this.state}
          refetchQueries={[{ query: USER_CURRENT_QUERY }]}
        >
          {(signup, { loading, error }) => (
            <form
              className="col-lg-8 col-md-10"
              onSubmit={async e => {
                e.preventDefault();
                await signup();
                this.setState({ name: "", email: "", password: "" });
              }}
            >
              <DisplayError error={error} />

              <fieldset disabled={loading}>
                <div className="form-group custom">
                  <label>Имя</label>
                  <input
                    name="name"
                    type="text"
                    className="form-control"
                    placeholder="Имя"
                    required
                    value={this.state.name}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="form-group custom">
                  <label>Email</label>
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    required
                    value={this.state.email}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="form-group custom">
                  <label>Пароль</label>
                  <input
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="Пароль"
                    required
                    value={this.state.password}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="text-center">
                  <button className="btn btn-outline-danger" type="submit">
                    {loading && <Spiner />}
                    Регистрация
                  </button>
                </div>
              </fieldset>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}
