import React, { Component } from "react";
import { Mutation } from "react-apollo";
import DisplayError from "../ErrorMessage";
import Spiner from "../Spiner";
import {
  USER_RESET_PASSWORD_MUTATION,
  USER_CURRENT_QUERY
} from "../../lib/queries";
import PropTypes from "prop-types";
import Router from "next/router";

export default class ResetPasswordForm extends Component {
  state = {
    password: "",
    confirmPassword: ""
  };
  saveToState = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  static propTypes = {
    resetToken: PropTypes.string.isRequired
  };

  render() {
    return (
      <div className="row mt-4 flex justify-content-center">
        <Mutation
          mutation={USER_RESET_PASSWORD_MUTATION}
          refetchQueries={[{ query: USER_CURRENT_QUERY }]}
          variables={{
            ...this.state,
            resetToken: this.props.resetToken
          }}
        >
          {(resetPassword, { loading, error, called }) => (
            <form
              className="col-lg-8 col-md-10"
              onSubmit={async e => {
                e.preventDefault();
                await resetPassword();
                this.setState({ password: "", confirmPassword: "" });
                Router.push({
                  pathname: "/"
                });
              }}
            >
              <DisplayError error={error} />

              {!error && !loading && called && (
                <div className="success-display">
                  <p data-test="graphql-success">Пароль успешно сменен</p>
                </div>
              )}

              <fieldset disabled={loading}>
                <div className="form-group custom">
                  <label>Пароль</label>
                  <input
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="Пароль"
                    required
                    value={this.state.password}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="form-group custom">
                  <label>Повтор пароля</label>
                  <input
                    name="confirmPassword"
                    type="password"
                    className="form-control"
                    placeholder="Повтор пароля"
                    required
                    value={this.state.confirmPassword}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="text-center">
                  <button className="btn btn-outline-danger" type="submit">
                    {loading && <Spiner />}
                    Подтвердить
                  </button>
                </div>
              </fieldset>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}
