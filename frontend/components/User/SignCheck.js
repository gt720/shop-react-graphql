import { Query } from "react-apollo";
import { USER_CURRENT_QUERY } from "../../lib/queries";
import SigninForm from "./SigninForm";
const SignCheck = props => (
  <Query query={USER_CURRENT_QUERY}>
    {({ data, error, loading }) => {
      if (loading) return <p>Loading ...</p>;
      if (error) return <p>Error: {error.message}</p>;
      if (!data.me)
        return (
          <>
            <div className="row mt-4 flex justify-content-center">
              <div className="col text-center">
                <p>Вам необходимо авторизоваться, что бы продолжить</p>
              </div>
            </div>
            <SigninForm redirect={false} />
          </>
        );
      return props.children;
    }}
  </Query>
);

export default SignCheck;
