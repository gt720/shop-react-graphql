import React, { Component } from "react";
import { Mutation } from "react-apollo";
import DisplayError from "../ErrorMessage";
import Spiner from "../Spiner";
import { USER_RESET_REQUEST_MUTATION } from "../../lib/queries";

export default class ResetRequestForm extends Component {
  state = {
    email: ""
  };
  saveToState = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="row mt-4 flex justify-content-center">
        <Mutation mutation={USER_RESET_REQUEST_MUTATION} variables={this.state}>
          {(resetRequest, { loading, error, called }) => (
            <form
              className="col-lg-8 col-md-10"
              onSubmit={async e => {
                e.preventDefault();
                await resetRequest();
                this.setState({ email: "" });
              }}
            >
              <DisplayError error={error} />

              {!error && !loading && called && (
                <div className="success-display">
                  <p data-test="graphql-success">
                    Письмо с инструкциями отправлена на вашу почту
                  </p>
                </div>
              )}

              <fieldset disabled={loading}>
                <div className="form-group custom">
                  <label>Email</label>
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    required
                    value={this.state.email}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="text-center">
                  <button className="btn btn-outline-danger" type="submit">
                    {loading && <Spiner />}
                    Сбросить пароль
                  </button>
                </div>
              </fieldset>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}
