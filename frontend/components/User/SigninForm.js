import React, { Component } from "react";
import { Mutation } from "react-apollo";
import DisplayError from "../ErrorMessage";
import Spiner from "../Spiner";
import { USER_SIGNIN_MUTATION, USER_CURRENT_QUERY } from "../../lib/queries";
import Router from "next/router";

export default class SigninForm extends Component {
  state = {
    email: "",
    password: ""
  };
  saveToState = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="row mt-4 flex justify-content-center">
        <Mutation
          mutation={USER_SIGNIN_MUTATION}
          variables={this.state}
          refetchQueries={[{ query: USER_CURRENT_QUERY }]}
        >
          {(signin, { loading, error }) => (
            <form
              className="col-lg-8 col-md-10"
              onSubmit={async e => {
                e.preventDefault();
                await signin();
                this.setState({ email: "", password: "" });

                if (!error && this.props.redirect) {
                  Router.push({
                    pathname: "/"
                  });
                }
              }}
            >
              <DisplayError error={error} />

              <fieldset disabled={loading}>
                <div className="form-group custom">
                  <label>Email</label>
                  <input
                    name="email"
                    type="email"
                    className="form-control"
                    placeholder="Email"
                    required
                    value={this.state.email}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="form-group custom">
                  <label>Пароль</label>
                  <input
                    name="password"
                    type="password"
                    className="form-control"
                    placeholder="Пароль"
                    required
                    value={this.state.password}
                    onChange={this.saveToState}
                  />
                </div>

                <div className="text-center">
                  <button className="btn btn-outline-danger" type="submit">
                    {loading && <Spiner />}
                    Вход
                  </button>
                </div>
              </fieldset>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}
