import { Query } from "react-apollo";
import { USER_CURRENT_QUERY } from "../../lib/queries";
import PropTypes from "prop-types";

const User = props => (
  <Query {...props} query={USER_CURRENT_QUERY}>
    {payload => props.children(payload)}
  </Query>
);

User.propTypes = {
  children: PropTypes.func.isRequired
};

export default User;
