import { Query, Mutation } from "react-apollo";
import {
  USER_USERS_QUERY,
  USER_UPDATE_PERMISSIONS_MUTATIONS,
  USER_CURRENT_QUERY
} from "../../lib/queries";
import DisplayError from "./../ErrorMessage";
import PropTypes from "prop-types";

const possiblePermissions = [
  "ADMIN",
  "USER",
  "ITEMCREATE",
  "ITEMUPDATE",
  "ITEMDELETE",
  "PERMISSIONUPDATE"
];

const Permissions = props => (
  <Query query={USER_USERS_QUERY}>
    {({ data, loading, error }) => {
      if (loading) return <p>Loading...</p>;
      if (error) return <DisplayError error={error} />;

      return (
        <div className="row mt-4 flex justify-content-center">
          <div className="col-md-12">
            <h3 className="text-center mb-5">Разрешения пользователей</h3>
          </div>
          <div className="col-md-12">
            <table className="table md-table-responsive">
              <thead>
                <tr>
                  <th scope="col">Имя</th>
                  <th scope="col">Email</th>
                  {possiblePermissions.map(permission => (
                    <th scope="col" key={permission}>
                      {permission}
                    </th>
                  ))}
                  <th scope="col" />
                </tr>
              </thead>
              <tbody>
                {data.users.map(user => (
                  <PermissionsTable user={user} key={user.id} />
                ))}
              </tbody>
            </table>
          </div>
        </div>
      );
    }}
  </Query>
);

class PermissionsTable extends React.Component {
  static propTypes = {
    user: PropTypes.shape({
      name: PropTypes.string,
      email: PropTypes.string,
      id: PropTypes.string,
      permissions: PropTypes.array
    }).isRequired
  };
  state = {
    permissions: this.props.user.permissions
  };
  handlePermissionChange = e => {
    const checkbox = e.target;

    let permissions = [...this.state.permissions];

    if (checkbox.checked) {
      permissions.push(checkbox.value);
    } else {
      permissions = permissions.filter(item => item != checkbox.value);
    }

    this.setState({ permissions });
  };

  render() {
    const user = this.props.user;
    return (
      <Mutation
        mutation={USER_UPDATE_PERMISSIONS_MUTATIONS}
        variables={{ ...this.state, id: user.id }}
        // refetchQueries={[{ query: USER_CURRENT_QUERY }]}
      >
        {(updatePermissions, { loading, error, called, data }) => (
          <>
            <DisplayError error={error} />
            <tr>
              <td>{user.name}</td>
              <td>{user.email}</td>
              {possiblePermissions.map(permission => (
                <td key={permission} className="text-center">
                  <label htmlFor={`${user.id}-permission-${permission}`}>
                    <input
                      id={`${user.id}-permission-${permission}`}
                      type="checkbox"
                      checked={this.state.permissions.includes(permission)}
                      value={permission}
                      onChange={this.handlePermissionChange}
                    />
                  </label>
                </td>
              ))}
              <td>
                <button
                  className="btn btn-outline-danger btn-sm"
                  disabled={loading}
                  onClick={async e => {
                    e.preventDefault();
                    await updatePermissions();
                  }}
                >
                  Обновить
                </button>
              </td>
            </tr>
          </>
        )}
      </Mutation>
    );
  }
}

export default Permissions;
