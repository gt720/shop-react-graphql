import React, { Component } from "react";
import { Query } from "react-apollo";
import Item from "./Item";
import { ITEM_ALL_ITEMS_QUERY } from "../../lib/queries";
import Pagination from "./Pagination";
import { perPage } from "../../config";

const Items = props => (
  <div className="container pt-4">
    <Pagination page={props.page} />
    <div className="row">
      <Query
        query={ITEM_ALL_ITEMS_QUERY}
        // fetchPolicy="network-only"
        variables={{ skip: (props.page - 1) * perPage }}
      >
        {({ data, error, loading }) => {
          if (loading) return <p>Loading ...</p>;
          if (error) return <p>Error: {error.message}</p>;
          return data.items.map(item => <Item key={item.id} {...item} />);
        }}
      </Query>
    </div>
    <Pagination page={props.page} />
  </div>
);

export default Items;
