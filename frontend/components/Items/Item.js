import Link from "next/link";
import formatMoney from "../../lib/formatMoney";
import DeleteItem from "./DeleteItem";
import CartAddItem from "./../Cart/CartAddItem";

const Item = props => (
  <div className="col-sm-12 col-lg-6 pt-4 d-flex justify-content-center">
    <div className="card card_item mb-5">
      {props.image && (
        <div className="card_image_wrapper">
          <img src={props.image} className="card-img-top" alt={props.title} />
        </div>
      )}
      <div className="card-body">
        <div className="card-title-wrapper">
          <Link href={{ pathname: "/item", query: { id: props.id } }}>
            <a className="card-title">{props.title}</a>
          </Link>
        </div>
        <div className="card-price">{formatMoney(props.price)}</div>

        <div className="card-text text-center pt-4">{props.description}</div>
      </div>
      <div className="card-footer d-flex justify-content-between">
        <Link href={{ pathname: "/update", query: { id: props.id } }}>
          <a className="btn btn-outline-danger">Редактировать</a>
        </Link>
        <CartAddItem id={props.id} />
        <DeleteItem id={props.id}>Удалить</DeleteItem>
      </div>
    </div>
  </div>
);

export default Item;
