import { ITEM_PAGINATION } from "../../lib/queries";
import { Query } from "react-apollo";
import { perPage } from "../../config";
import Link from "next/link";

const Pagination = ({ page }) => (
  <Query query={ITEM_PAGINATION}>
    {({ data, error, loading }) => {
      if (loading) return <p>Loading ...</p>;
      if (error) return <p>Error: {error.message}</p>;
      let count = data.itemsConnection.aggregate.count;
      let pages = Math.ceil(count / perPage);

      return (
        pages > 1 && (
          <div className="row mb-4">
            <div className="col">
              <nav aria-label="Page navigation example">
                <ul className="pagination justify-content-center">
                  <li className={`page-item ${page <= 1 ? "disabled" : ""}`}>
                    <Link
                      href={{
                        pathname: "items",
                        query: { page: page - 1 }
                      }}
                    >
                      <a className="page-link" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span className="sr-only">Previous</span>
                      </a>
                    </Link>
                  </li>

                  {pages > 1 && (
                    <li className="page-item">
                      <p className="page-link">{`Страница ${page} из ${pages}`}</p>
                    </li>
                  )}

                  {pages > 1 && (
                    <li
                      className={`page-item ${page >= pages ? "disabled" : ""}`}
                    >
                      <Link
                        href={{
                          pathname: "items",
                          query: { page: page + 1 }
                        }}
                      >
                        <a className="page-link" href="#" aria-label="Next">
                          <span aria-hidden="true">&raquo;</span>
                          <span className="sr-only">Next</span>
                        </a>
                      </Link>
                    </li>
                  )}
                </ul>
              </nav>
            </div>
          </div>
        )
      );
    }}
  </Query>
);

export default Pagination;
