import React, { Component } from "react";
import { Mutation } from "react-apollo";
import gql from "graphql-tag";
import formatMoney from "../../lib/formatMoney";
import DisplayError from "../ErrorMessage";
import Router from "next/router";
import Spiner from "../Spiner";
import { ITEM_CREATE_MUTATION } from "../../lib/queries";

export default class CreateItem extends Component {
  state = {
    title: "Классные кеды",
    description: "Кеды из натуральной кожи",
    image: "",
    largeImage: "",
    price: 5000,
    imageLoading: false
  };

  handleChange = e => {
    const { name, type, value } = e.target;
    let val =
      type === "number"
        ? isNaN(parseFloat(value))
          ? 0
          : parseFloat(value)
        : value;
    this.setState({ [name]: val });
  };

  uploadImage = async e => {
    this.setState({ imageLoading: true });

    const files = e.target.files;
    const form = new FormData();
    form.append("file", files[0]);
    form.append("upload_preset", "next-shop");

    const res = await fetch(
      `https://api.cloudinary.com/v1_1/teimur8/image/upload`,
      {
        method: "POST",
        body: form
      }
    );

    const file = await res.json();
    console.log(file);
    this.setState({
      image: file.secure_url,
      largeImage: file.eager[0].secure_url,
      imageLoading: false
    });
  };

  render() {
    return (
      <div className="row mt-4 flex justify-content-center">
        <Mutation
          // refetchQueries={}
          mutation={ITEM_CREATE_MUTATION}
          variables={this.state}
        >
          {(createItem, { loading, error, called, data }) => (
            <form
              className="col-lg-8 col-md-10"
              onSubmit={async e => {
                e.preventDefault();
                const res = await createItem();
                Router.push({
                  pathname: "/item",
                  query: { id: res.data.createItem.id }
                });
              }}
            >
              <DisplayError error={error} />

              <fieldset disabled={loading || this.state.imageLoading}>
                {this.state.image && (
                  <div className="form-image w-25">
                    <img className="w-100" src={this.state.image} />
                  </div>
                )}
                <div className="form-group custom">
                  <label>
                    {this.state.imageLoading && <Spiner />}
                    Изображение
                  </label>

                  <input
                    name="file"
                    type="file"
                    className="form-control-file form-control"
                    onChange={this.uploadImage}
                  />
                </div>

                <div className="form-group custom">
                  <label>Название</label>
                  <input
                    name="title"
                    type="text"
                    className="form-control"
                    placeholder="Название"
                    value={this.state.title}
                    onChange={this.handleChange}
                  />
                </div>

                <div className="form-group custom">
                  <label>Цена</label>
                  <input
                    name="price"
                    type="number"
                    className="form-control"
                    placeholder="Цена"
                    value={this.state.price}
                    onChange={this.handleChange}
                  />
                </div>

                <div className="form-group custom">
                  <label>Описание</label>
                  <textarea
                    name="description"
                    className="form-control"
                    placeholder="Описание"
                    value={this.state.description}
                    onChange={this.handleChange}
                  />
                </div>

                <div className="text-center">
                  <button className="btn btn-outline-danger" type="submit">
                    {loading && <Spiner />}
                    Готово
                  </button>
                </div>
              </fieldset>
            </form>
          )}
        </Mutation>
      </div>
    );
  }
}
