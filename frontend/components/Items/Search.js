import React, { Component } from "react";
import { ApolloConsumer } from "react-apollo";
import { ITEM_SEARCH_QUERY } from "../../lib/queries";
import debounce from "lodash.debounce";
import Downshift, { resetIdCounter } from "downshift";
import Router from "next/router";

function routeToItem(item) {
  Router.push({
    pathname: "/item",
    query: {
      id: item.id
    }
  });
}

class Search extends React.Component {
  state = {
    items: [],
    loading: false
  };

  onChange = debounce(async (e, client) => {
    this.setState({ loading: true });

    const items = await client.query({
      query: ITEM_SEARCH_QUERY,
      variables: { search: e.target.value }
    });

    this.setState({
      items: items.data.items,
      loading: false
    });
  }, 350);

  render() {
    resetIdCounter();
    return (
      <Downshift
        itemToString={item => (item === null ? "" : item.title)}
        onChange={routeToItem}
      >
        {({
          getInputProps,
          getItemProps,
          isOpen,
          inputValue,
          highlightedIndex
        }) => (
          <div className="search">
            <ApolloConsumer>
              {client => (
                <input
                  {...getInputProps({
                    type: "text",
                    placeholder: "Поиск по товарам",
                    id: "search",
                    className: this.state.loading ? "loading" : "",
                    onChange: e => {
                      e.persist();
                      this.onChange(e, client);
                    }
                  })}
                />
              )}
            </ApolloConsumer>
            {isOpen && (
              <ul className="list-group">
                {this.state.items.map((item, index) => (
                  <li
                    {...getItemProps({ item })}
                    key={item.id}
                    className={`list-group-item ${
                      index === highlightedIndex ? "selected" : ""
                    }`}
                  >
                    <div className="d-flex justify-content-start">
                      <div className="img">
                        <img src={item.image} alt={item.title} />
                      </div>
                      <div className="title">{item.title}</div>
                    </div>
                  </li>
                ))}
                {!this.state.items.length && !this.state.loading && (
                  <li className="list-group-item">
                    <div className="d-flex justify-content-start">
                      <div className="title">Ничего не найдено</div>
                    </div>
                  </li>
                )}
              </ul>
            )}
          </div>
        )}
      </Downshift>
    );
  }
}

export default Search;
