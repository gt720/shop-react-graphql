import React, { Component } from "react";
import { Mutation } from "react-apollo";
import { ITEM_ALL_ITEMS_QUERY, ITEM_DELETE_MUTATION } from "../../lib/queries";

const update = (cache, payload) => {
  // manually update the cache on the client, so it matches the server
  // 1. Read the cache for the items we want
  const data = cache.readQuery({ query: ITEM_ALL_ITEMS_QUERY });
  console.log(data, payload);
  // 2. Filter the deleted itemout of the page
  data.items = data.items.filter(
    item => item.id !== payload.data.deleteItem.id
  );
  // 3. Put the items back!
  cache.writeQuery({ query: ITEM_ALL_ITEMS_QUERY, data });
};

const DeleteItem = ({ id, children }) => (
  <Mutation
    mutation={ITEM_DELETE_MUTATION}
    variables={{ id: id }}
    update={update}
  >
    {(deleteItem, { loading, error, called, data }) => {
      return (
        <button
          className="btn btn-outline-danger"
          onClick={e => {
            if (!confirm("Точно?")) return;
            deleteItem();
          }}
        >
          {children}
        </button>
      );
    }}
  </Mutation>
);
export default DeleteItem;
