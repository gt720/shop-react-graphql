import React, { Component } from "react";
import { Mutation, Query } from "react-apollo";
import DisplayError from "../ErrorMessage";
import Router from "next/router";
import Spiner from "../Spiner";
import { ITEM_SINGLE_QUERY, ITEM_UPDATE_MUTATION } from "../../lib/queries";

export default class UpdateItem extends Component {
  state = {};

  handleChange = e => {
    const { name, type, value } = e.target;
    let val =
      type === "number"
        ? isNaN(parseFloat(value))
          ? 0
          : parseFloat(value)
        : value;
    this.setState({ [name]: val });
  };

  submitForm = async (e, updateItemMutation) => {
    e.preventDefault();
    const res = await updateItemMutation({
      variables: {
        id: this.props.id,
        ...this.state
      }
    });
    Router.push({
      pathname: "/item",
      query: { id: res.data.updateItem.id }
    });
  };

  render() {
    return (
      <div className="row mt-4 flex justify-content-center">
        <Query
          query={ITEM_SINGLE_QUERY}
          variables={{
            id: this.props.id
          }}
        >
          {({ data, loading }) => {
            const item = data.item;
            if (loading) return <p>Loading...</p>;
            if (!item) return <p>Item not found...</p>;
            return (
              <Mutation mutation={ITEM_UPDATE_MUTATION} variables={item}>
                {(updateItem, { loading, error, called, data }) => (
                  <form
                    className="col-lg-8 col-md-10"
                    onSubmit={e => this.submitForm(e, updateItem)}
                  >
                    <DisplayError error={error} />

                    <fieldset disabled={loading}>
                      <div className="form-group custom">
                        <label>Название</label>
                        <input
                          name="title"
                          type="text"
                          className="form-control"
                          placeholder="Название"
                          defaultValue={item.title}
                          onChange={this.handleChange}
                        />
                      </div>

                      <div className="form-group custom">
                        <label>Цена</label>
                        <input
                          name="price"
                          type="number"
                          className="form-control"
                          placeholder="Цена"
                          defaultValue={item.price}
                          onChange={this.handleChange}
                        />
                      </div>

                      <div className="form-group custom">
                        <label>Описание</label>
                        <textarea
                          name="description"
                          className="form-control"
                          placeholder="Описание"
                          defaultValue={item.description}
                          onChange={this.handleChange}
                        />
                      </div>

                      <div className="text-center">
                        <button
                          className="btn btn-outline-danger"
                          type="submit"
                        >
                          {loading && <Spiner />}
                          Сохранить
                        </button>
                      </div>
                    </fieldset>
                  </form>
                )}
              </Mutation>
            );
          }}
        </Query>
      </div>
    );
  }
}
