import { ErrorsTost } from "../Errors";
import { Mutation } from "react-apollo";
import {
  CART_CREATE_ORDER_MUTATION,
  USER_CURRENT_QUERY
} from "../../lib/queries";
import Router from "next/router";

const CartCreateOrder = props => {
  return (
    <Mutation
      mutation={CART_CREATE_ORDER_MUTATION}
      refetchQueries={[{ query: USER_CURRENT_QUERY }]}
    >
      {(createOrder, { loading, error }) => (
        <>
          <ErrorsTost error={error} />
          <fieldset disabled={loading}>
            <button
              type="button"
              className="btn btn-outline-danger"
              onClick={async e => {
                if (!confirm("Точно?")) return;
                e.preventDefault();
                let order = await createOrder();
                Router.push({
                  pathname: "/order",
                  query: { id: order.data.createOrder.id }
                });
              }}
            >
              Оформить
            </button>
          </fieldset>
        </>
      )}
    </Mutation>
  );
};

export default CartCreateOrder;
