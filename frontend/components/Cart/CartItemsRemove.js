import { ErrorsTost } from "./../Errors";
import { Mutation } from "react-apollo";
import {
  CART_REMOVE_ITEM_MUTATION,
  USER_CURRENT_QUERY
} from "../../lib/queries";

const CartItemsRemove = ({ id }) => {
  const update = (cache, payload) => {
    const data = cache.readQuery({ query: USER_CURRENT_QUERY });
    data.me.cart = data.me.cart.filter(
      item => item.id !== payload.data.removeFromCart.id
    );
    cache.writeQuery({ query: USER_CURRENT_QUERY, data });
  };

  return (
    <Mutation
      mutation={CART_REMOVE_ITEM_MUTATION}
      variables={{ id }}
      // refetchQueries={[{ query: USER_CURRENT_QUERY }]} // можно так
      update={update} // или так. так быстрее
      optimisticResponse={{
        __typename: "Mutation",
        removeFromCart: {
          __typename: "CartItem",
          id
        }
      }}
    >
      {(removeFromCart, { loading, error }) => (
        <>
          <ErrorsTost error={error} />
          <fieldset disabled={loading}>
            <button
              type="button"
              className="btn btn-outline-danger btn-sm"
              onClick={e => {
                if (!confirm("Точно?")) return;
                e.preventDefault();
                removeFromCart();
              }}
            >
              <span>×</span>
            </button>
          </fieldset>
        </>
      )}
    </Mutation>
  );
};

export default CartItemsRemove;
