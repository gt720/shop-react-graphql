import React, { Component } from "react";
import { ORDER_SINGLE_QUERY } from "../../lib/queries";
import { Query } from "react-apollo";
import { format } from "date-fns";
import { es, ru } from "date-fns/locale";
import Head from "next/head";
import formatMoney from "../../lib/formatMoney";

export default class Order extends Component {
  render() {
    const { id } = this.props;

    return (
      <Query query={ORDER_SINGLE_QUERY} variables={{ id }}>
        {({ data, error, loading }) => {
          if (loading) return <p>Loading ...</p>;
          if (error) return <p>Error: {error.message}</p>;
          const order = data.order;
          return (
            <div className="order">
              <Head>
                <title>Sick Fits - Order {order.id}</title>
              </Head>
              <table className="table">
                <tbody>
                  <tr>
                    <th>Номер заказа</th>
                    <td>{this.props.id}</td>
                  </tr>
                  <tr>
                    <th>Дата</th>
                    <td>
                      {format(new Date(order.createdAt), "D MMMM YYYY, HH:mm", {
                        locale: ru,
                        awareOfUnicodeTokens: true
                      })}
                    </td>
                  </tr>
                  <tr>
                    <th>Сумма</th>
                    <td>{formatMoney(order.total)}</td>
                  </tr>
                  <tr>
                    <th>Количесвто</th>
                    <td>{order.items.length}</td>
                  </tr>
                </tbody>
              </table>

              <div className="items">
                {order.items.map(item => (
                  <div className="row" key={item.id}>
                    <div className="col-md-12">
                      <h2>{item.title}</h2>
                    </div>
                    <div className="col-md-4">
                      <img
                        src={item.image}
                        alt={item.title}
                        className="w-100"
                      />
                    </div>
                    <div className="col-md-8">
                      <p>
                        <b>Количество</b>: {item.quantity}
                      </p>
                      <p>
                        <b>Цена за штуку</b>: {formatMoney(item.price)}
                      </p>
                      <p>
                        <b>Сумма</b>: {formatMoney(item.price * item.quantity)}
                      </p>
                      <p>{item.description}</p>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}
