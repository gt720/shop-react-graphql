import React, { Component } from "react";
import {
  LOCAL_CART_OPEN_QUERY,
  LOCAL_CART_TOGGLE_MUTATION
} from "../../lib/queries";
import { Query, Mutation } from "react-apollo";
import User from "../User/User";
import CartItems from "./CartItems";
import formatMoney from "../../lib/formatMoney";
import calcTotalPrice from "./../../lib/calcTotalPrice";
import { adopt } from "react-adopt";
import CartCreateOrder from "./CartCreateOrder";

/* eslint-disable */
const Composed = adopt({
  user: ({ render }) => <User>{render}</User>,
  toggleCart: ({ render }) => (
    <Mutation mutation={LOCAL_CART_TOGGLE_MUTATION}>{render}</Mutation>
  ),
  localState: ({ render }) => (
    <Query query={LOCAL_CART_OPEN_QUERY}>{render}</Query>
  )
});
/* eslint-enable */

export default class Cart extends Component {
  render() {
    return (
      <Composed>
        {({ user, toggleCart, localState }) => {
          const me = user.data.me;
          if (!me) return null;

          return (
            <div
              className={`modal-content cart ${localState.data.cartOpen &&
                "open"}`}
            >
              <div className="modal-header">
                <h5 className="modal-title">Моя корзина</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => {
                    toggleCart();
                  }}
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <div className="row">
                  <CartItems cart={me.cart} />
                </div>
              </div>
              <div className="modal-footer flex justify-content-between">
                <span className="sum">
                  {formatMoney(calcTotalPrice(me.cart))}
                </span>
                {me.cart.length > 0 && <CartCreateOrder />}
              </div>
            </div>
          );
        }}
      </Composed>
    );
  }
}
