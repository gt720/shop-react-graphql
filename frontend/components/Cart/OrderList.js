import React, { Component } from "react";
import { ORDER_USERS_QUERY } from "../../lib/queries";
import { Query } from "react-apollo";
import { formatDistance } from "date-fns";
import { es, ru } from "date-fns/locale";
import formatMoney from "../../lib/formatMoney";
import Link from "next/link";

export default class OrderList extends Component {
  render() {
    return (
      <Query query={ORDER_USERS_QUERY}>
        {({ data, error, loading }) => {
          if (loading) return <p>Loading ...</p>;
          if (error) return <p>Error: {error.message}</p>;
          const orders = data.orders;

          return (
            <div className="row">
              <div className="col-md-12">
                <h2 className="text-center mt-2 mb-4">
                  У вас {orders.length} заказ(ов)
                </h2>
              </div>
              <div className="col-md-12">
                {orders.map(order => (
                  <div className="row" key={order.id}>
                    <div className="col-md-2">
                      {order.items.reduce((a, b) => a + b.quantity, 0)} штук
                    </div>
                    <div className="col-md-2">{order.items.length} позиции</div>
                    <div className="col-md-2">
                      {formatDistance(new Date(order.createdAt), new Date(), {
                        locale: ru,
                        awareOfUnicodeTokens: true
                      })}
                    </div>
                    <div className="col-md-2">
                      На сумму {formatMoney(order.total)}
                    </div>
                    <div className="col-md-2">
                      {order.items.map(item => (
                        <img
                          className="w-25"
                          key={item.id}
                          src={item.image}
                          alt={item.title}
                        />
                      ))}
                    </div>
                    <div className="col-md-2">
                      <Link
                        href={{
                          pathname: "/order",
                          query: { id: order.id }
                        }}
                      >
                        <a className="btn btn-outline-danger btn-sm">
                          Подробнее
                        </a>
                      </Link>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}
