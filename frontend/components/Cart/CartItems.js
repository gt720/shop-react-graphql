import CartItemsRemove from "./CartItemsRemove";
import formatMoney from "../../lib/formatMoney";

const CartItems = ({ cart }) => {
  return (
    <>
      {cart.map(item => {
        if (!item.item) {
          return (
            <div className="col-md-12" key={item.id}>
              <div className="row">
                <div className="col-md-9">
                  <p>Товар удален</p>
                </div>
                <div className="col-md-3 d-flex justify-content-center align-items-center">
                  <CartItemsRemove id={item.id} />
                </div>
              </div>
            </div>
          );
        }

        return (
          <div className="col-md-12" key={item.id}>
            <div className="row">
              <div className="col-md-3">
                <p className="img">
                  <img src={item.item.image} />
                </p>
              </div>
              <div className="col-md-6">
                <p className="title">
                  <b>{item.item.title}</b>
                </p>
                <p className="price">
                  {item.quantity}
                  <i> шт</i> <b>*</b> {formatMoney(item.item.price)} ={" "}
                  {formatMoney(item.quantity * item.item.price)}
                </p>
              </div>
              <div className="col-md-3 d-flex justify-content-center align-items-center">
                <CartItemsRemove id={item.id} />
              </div>
            </div>
          </div>
        );
      })}
    </>
  );
};

export default CartItems;
