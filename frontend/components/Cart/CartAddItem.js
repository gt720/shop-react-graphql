import { Mutation } from "react-apollo";
import { CART_ADD_ITEM_MUTATION, USER_CURRENT_QUERY } from "../../lib/queries";
import { ErrorsTost } from "./../Errors";

const CartAddItem = props => {
  const { id } = props;

  return (
    <Mutation
      mutation={CART_ADD_ITEM_MUTATION}
      variables={{ id }}
      refetchQueries={[{ query: USER_CURRENT_QUERY }]}
    >
      {(addToCart, { loading, error, called, data }) => (
        <>
          <ErrorsTost error={error} />
          <fieldset disabled={loading}>
            <button
              className="btn btn-outline-danger"
              onClick={e => {
                e.preventDefault();
                addToCart();
              }}
            >
              В корзину
            </button>
          </fieldset>
        </>
      )}
    </Mutation>
  );
};

export default CartAddItem;
