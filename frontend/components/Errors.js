import React, { Component } from "react";
import { toast } from "react-toastify";

class Errors extends React.Component {
  render() {
    const { error } = this.props;

    if (!error || !error.message) return null;

    if (
      error.networkError &&
      error.networkError.result &&
      error.networkError.result.errors.length
    ) {
      error.networkError.result.errors.map((error, i) => {
        toast.error(error.message.replace("GraphQL error: ", ""), {
          position: toast.POSITION.TOP_RIGHT
        });
      });
      return null;
    }

    toast.error(error.message.replace("GraphQL error: ", ""), {
      position: toast.POSITION.TOP_RIGHT
    });

    return null;
  }
}

export const ErrorsTost = Errors;
