const Spiner = porps => (
  <span
    className="spinner-border spinner-border-sm mr-2"
    role="status"
    aria-hidden="true"
  />
);
export default Spiner;
