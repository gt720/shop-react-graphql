import CreateItem from "./../components/Items/CreateItem";
import SignCheck from "./../components/User/SignCheck";

const Sell = props => (
  <div className="container">
    <SignCheck>
      <CreateItem />
    </SignCheck>
  </div>
);

export default Sell;
