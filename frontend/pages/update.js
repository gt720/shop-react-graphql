import UpdateItem from "./../components/Items/UpdateItem";

export default ({ query }) => (
  <div className="container">
    <UpdateItem id={query.id} />
  </div>
);
