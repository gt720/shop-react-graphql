import SignupForm from "./../components/User/SignupForm";
const Signup = props => (
  <div className="container">
    <div className="row mt-4 flex justify-content-center">
      <div className="col text-center">
        <h4>Регистрация</h4>
      </div>
    </div>
    <SignupForm />
  </div>
);

export default Signup;
