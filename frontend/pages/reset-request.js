import ResetRequestForm from "./../components/User/ResetRequestForm";

const ResetRequestPage = props => (
  <div className="container">
    <div className="row mt-4 flex justify-content-center">
      <div className="col text-center">
        <h4>Запрос на восстановление</h4>
      </div>
    </div>
    <ResetRequestForm />
  </div>
);

export default ResetRequestPage;
