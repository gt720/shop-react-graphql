import ResetPasswordForm from "./../components/User/ResetPasswordForm";
const ResetPasswordPage = ({ query }) => (
  <div className="container">
    <div className="row mt-4 flex justify-content-center">
      <div className="col text-center">
        <h4>Введите новый пароль</h4>
      </div>
    </div>
    <ResetPasswordForm resetToken={query.resetToken} />
  </div>
);

export default ResetPasswordPage;
