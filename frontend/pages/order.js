import SignCheck from "./../components/User/SignCheck";
import Order from "./../components/Cart/Order";

const OrderPage = ({ query }) => (
  <div className="container">
    <SignCheck>
      <Order id={query.id} />
    </SignCheck>
  </div>
);

export default OrderPage;
