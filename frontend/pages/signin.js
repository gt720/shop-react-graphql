import SigninForm from "./../components/User/SigninForm";
const Signup = props => (
  <div className="container">
    <div className="row mt-4 flex justify-content-center">
      <div className="col text-center">
        <h4>Вход</h4>
      </div>
    </div>
    <SigninForm />
  </div>
);

export default Signup;
