import ShowItem from "./../components/Items/ShowItem";
import { ITEM_SINGLE_QUERY } from "../lib/queries";
import { Query } from "react-apollo";
import Head from "next/head";

export default ({ query }) => (
  <div className="container">
    <div className="raw pt-4">
      <Query query={ITEM_SINGLE_QUERY} variables={{ id: query.id }}>
        {({ data, error, loading }) => {
          if (loading) return <p>Loading ...</p>;
          if (error) return <p>Error: {error.message}</p>;
          if (!data.item) return <p>Item not found</p>;
          return (
            <div>
              <Head>
                <title>Next Shop | {data.item.title}</title>
              </Head>
              <ShowItem {...data.item} />
            </div>
          );
        }}
      </Query>
    </div>
  </div>
);
