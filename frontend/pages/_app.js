import App, { Container } from "next/app";
import Page from "./../components/Page";
import { ApolloProvider } from "react-apollo";
import withApollo from "./../lib/withData";

class MyApp extends App {
  // https://habr.com/ru/post/323588/ об getInitialProps
  // run before render function
  static async getInitialProps({ Component, ctx }) {
    let pageProps = {};
    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }
    pageProps.query = ctx.query;
    return { pageProps };
  }

  render() {
    const { Component, apollo, pageProps } = this.props;
    return (
      <Container>
        <ApolloProvider client={apollo}>
          <Page>
            <Component {...pageProps} />
          </Page>
        </ApolloProvider>
      </Container>
    );
  }
}

export default withApollo(MyApp);
