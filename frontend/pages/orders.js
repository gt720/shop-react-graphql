import SignCheck from "../components/User/SignCheck";
import OrderList from "../components/Cart/OrderList";

const OrderPage = props => (
  <div className="container">
    <SignCheck>
      <OrderList />
    </SignCheck>
  </div>
);

export default OrderPage;
