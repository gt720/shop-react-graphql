import gql from "graphql-tag";
import { perPage } from "../config";
import Permissions from "./../components/User/Permissions";

//#region ITEMS

const ITEM_ALL_ITEMS_QUERY = gql`
  query ITEM_ALL_ITEMS_QUERY($skip: Int = 0, $first: Int = ${perPage}) {
    items(skip: $skip, first: $first, orderBy: createdAt_DESC) {
      id
      title
      description
      image
      largeImage
      price
    }
  }
`;

const ITEM_SINGLE_QUERY = gql`
  query ITEM_SINGLE_QUERY($id: ID!) {
    item(where: { id: $id }) {
      id
      title
      description
      price
      image
      largeImage
    }
  }
`;

const ITEM_UPDATE_MUTATION = gql`
  mutation ITEM_UPDATE_MUTATION(
    $id: ID!
    $title: String!
    $description: String!
    $price: Int!
  ) {
    updateItem(
      id: $id
      title: $title
      description: $description
      price: $price
    ) {
      id
    }
  }
`;

const ITEM_CREATE_MUTATION = gql`
  mutation ITEM_CREATE_MUTATION(
    $title: String!
    $description: String!
    $image: String
    $largeImage: String
    $price: Int!
  ) {
    createItem(
      title: $title
      description: $description
      image: $image
      largeImage: $largeImage
      price: $price
    ) {
      id
    }
  }
`;

const ITEM_DELETE_MUTATION = gql`
  mutation ITEM_DELETE_MUTATION($id: ID!) {
    deleteItem(id: $id) {
      id
    }
  }
`;

const ITEM_PAGINATION = gql`
  query ITEM_PAGINATION {
    itemsConnection {
      aggregate {
        count
      }
    }
  }
`;

const ITEM_SEARCH_QUERY = gql`
  query ITEM_SEARCH_QUERY($search: String!) {
    items(
      where: {
        OR: [{ title_contains: $search }, { description_contains: $search }]
      }
    ) {
      id
      image
      title
    }
  }
`;

export { ITEM_SEARCH_QUERY };
export { ITEM_ALL_ITEMS_QUERY };
export { ITEM_SINGLE_QUERY };
export { ITEM_UPDATE_MUTATION };
export { ITEM_CREATE_MUTATION };
export { ITEM_DELETE_MUTATION };
export { ITEM_PAGINATION };

//#endregion

//#region User
const USER_SIGNUP_MUTATION = gql`
  mutation USER_SIGNUP_MUTATION(
    $email: String!
    $name: String!
    $password: String!
  ) {
    signup(email: $email, name: $name, password: $password) {
      id
      email
      name
    }
  }
`;

const USER_SIGNIN_MUTATION = gql`
  mutation USER_SIGNIN_MUTATION($email: String!, $password: String!) {
    signin(email: $email, password: $password) {
      id
      email
      name
    }
  }
`;

const USER_SIGNOUT_MUTATION = gql`
  mutation USER_SIGNOUT_MUTATION {
    signout {
      message
    }
  }
`;

const USER_CURRENT_QUERY = gql`
  query USER_CURRENT_QUERY {
    me {
      id
      email
      name
      permissions
      cart {
        id
        quantity
        item {
          id
          title
          price
          image
        }
      }
    }
  }
`;

const USER_RESET_REQUEST_MUTATION = gql`
  mutation USER_RESET_REQUEST_MUTATION($email: String!) {
    resetRequest(email: $email) {
      message
    }
  }
`;

const USER_RESET_PASSWORD_MUTATION = gql`
  mutation USER_RESET_PASSWORD_MUTATION(
    $resetToken: String!
    $password: String!
    $confirmPassword: String!
  ) {
    resetPassword(
      resetToken: $resetToken
      password: $password
      confirmPassword: $confirmPassword
    ) {
      id
      email
      name
      permissions
    }
  }
`;

const USER_USERS_QUERY = gql`
  query USER_USERS_QUERY {
    users {
      id
      name
      email
      permissions
    }
  }
`;

const USER_UPDATE_PERMISSIONS_MUTATIONS = gql`
  mutation USER_UPDATE_PERMISSIONS_MUTATIONS(
    $permissions: [Permission!]!
    $id: ID!
  ) {
    updatePermissions(permissions: $permissions, id: $id) {
      id
      name
      email
      permissions
    }
  }
`;

export { USER_UPDATE_PERMISSIONS_MUTATIONS };
export { USER_USERS_QUERY };
export { USER_RESET_REQUEST_MUTATION };
export { USER_RESET_PASSWORD_MUTATION };
export { USER_SIGNUP_MUTATION };
export { USER_SIGNIN_MUTATION };
export { USER_SIGNOUT_MUTATION };
export { USER_CURRENT_QUERY };
//#endregion

//#region local state

const LOCAL_CART_OPEN_QUERY = gql`
  query LOCAL_CART_OPEN_QUERY {
    cartOpen @client
  }
`;

const LOCAL_CART_TOGGLE_MUTATION = gql`
  mutation LOCAL_CART_TOGGLE_MUTATION {
    toggleCart @client
  }
`;

export { LOCAL_CART_OPEN_QUERY };
export { LOCAL_CART_TOGGLE_MUTATION };

//#endregion

//#region cart
const CART_ADD_ITEM_MUTATION = gql`
  mutation CART_ADD_ITEM_MUTATION($id: ID!) {
    addToCart(id: $id) {
      id
      quantity
      item {
        id
      }
      user {
        id
      }
    }
  }
`;

const CART_REMOVE_ITEM_MUTATION = gql`
  mutation CART_REMOVE_ITEM_MUTATION($id: ID!) {
    removeFromCart(id: $id) {
      id
      quantity
      quantity
    }
  }
`;

const CART_CREATE_ORDER_MUTATION = gql`
  mutation CART_CREATE_ORDER_MUTATION($userId: ID) {
    createOrder(userId: $userId) {
      id
      total
      items {
        id
        title
      }
    }
  }
`;

const ORDER_SINGLE_QUERY = gql`
  query ORDER_SINGLE_QUERY($id: ID!) {
    order(id: $id) {
      id
      total
      createdAt
      user {
        id
      }
      items {
        id
        title
        description
        price
        image
        quantity
      }
    }
  }
`;

const ORDER_USERS_QUERY = gql`
  query ORDER_USERS_QUERY {
    orders(orderBy: createdAt_DESC) {
      id
      total
      createdAt
      items {
        id
        title
        price
        description
        quantity
        image
      }
    }
  }
`;

export { ORDER_USERS_QUERY };
export { ORDER_SINGLE_QUERY };
export { CART_CREATE_ORDER_MUTATION };
export { CART_ADD_ITEM_MUTATION };
export { CART_REMOVE_ITEM_MUTATION };

//#endregion
